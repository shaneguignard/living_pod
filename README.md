# Nomadic lifepod

## 1. General Systems
- Environment inside and outside
    - Weather (Air Quality, Temperature, Rain, Storm, Etc...)
    - Noise Level
    - High Definition 360 video broadcast (outside-only)
    - Access key usage logging and monitoring
- Energy consumption and production
    - Power Generation
    - Power Usage
    - Solar Potential
    - Thermal Potential
    - Wind Potential
    - Battery level
- Water consumption and production
    - Fresh Tank Level
    - Grey Tank Level
    - Mineral Contents
    - Bateria (Nitrates) Contents
    - Quailty Contents
    - Temperature
- Waste production and disposal
    - Black Tank Level
    - Black Tank internal temperature
    - Incinerator
        - Temperature
        - Status
        - Timer

## 2. Bathroom
### 2.1 Toilet
- Access to Grey
- Drain to Black
### 2.2 Shower
- Access to grey
- Drain to grey


## 3. Kitchen
### 3.1 Cooking
- Exhaust Vent
- Propane Stove
- Bread Machine
- Rice Cooker
### 3.2 Storage
- Cold Food
- Dry Food
### 3.3 Sink
- Access to grey
- Drain to grey
### 3.4 Drink Tap
- Access to Fresh


## 4.Bedroom
- Sleeping
- Lighting
- Power Outlets
- Storage
    - Clothing
    - Jewerlery
    - Blankets

## Mainroom
- Map
    - Elevation
    - Latitude
    - Longitude    
- Communications broadcasts
    - VHF, UHF, HF, etc...
    - Internet

- Dinning Table
- Gaming Consoles
- Power Charging Station



