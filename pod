<mxfile host="app.diagrams.net" modified="2024-02-27T22:01:43.153Z" agent="Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/122.0.0.0 Safari/537.36" etag="apbxS_G7QLCglL0yPSYY" version="21.7.2" type="gitlab" pages="2">
  <diagram name="General Layout" id="tFWKzhCL0bpvEPOMqDHP">
    <mxGraphModel dx="1050" dy="570" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="850" pageHeight="1100" math="0" shadow="0">
      <root>
        <mxCell id="0" />
        <mxCell id="1" parent="0" />
        <mxCell id="s75VNq6IoDT7e8uqfi6e-1" value="Wheel" style="ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=#647687;fontColor=#ffffff;strokeColor=#314354;" parent="1" vertex="1">
          <mxGeometry x="425" y="650" width="70" height="70" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-3" value="Trailer Frame" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;" parent="1" vertex="1">
          <mxGeometry x="20" y="610" width="620" height="20" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-4" value="Wheel" style="ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=#647687;fontColor=#ffffff;strokeColor=#314354;" parent="1" vertex="1">
          <mxGeometry x="350" y="650" width="70" height="70" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-5" value="Water" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#dae8fc;strokeColor=#6c8ebf;" parent="1" vertex="1">
          <mxGeometry x="160" y="550" width="150" height="50" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-6" value="Energy" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#f8cecc;strokeColor=#b85450;" parent="1" vertex="1">
          <mxGeometry x="320" y="550" width="290" height="50" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-7" value="Flooring" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#fff2cc;strokeColor=#d6b656;" parent="1" vertex="1">
          <mxGeometry x="160" y="520" width="450" height="20" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-8" value="Polyfoam" style="rounded=0;whiteSpace=wrap;html=1;verticalAlign=middle;horizontal=0;fillColor=#ffe6cc;strokeColor=#d79b00;" parent="1" vertex="1">
          <mxGeometry x="130" y="200" width="20" height="400" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-9" value="Original Trailer Doors" style="rounded=0;whiteSpace=wrap;html=1;verticalAlign=middle;horizontal=0;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;" parent="1" vertex="1">
          <mxGeometry x="620" y="160" width="20" height="450" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-10" value="Natural&lt;br&gt;Gas" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#e51400;fontColor=#ffffff;strokeColor=#B20000;" parent="1" vertex="1">
          <mxGeometry x="40" y="500" width="50" height="100" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-11" value="Sleeping Area&amp;nbsp;&lt;br&gt;&lt;br&gt;8&#39; x 5&#39; x 4&#39;&amp;nbsp;" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#e1d5e7;strokeColor=#9673a6;" parent="1" vertex="1">
          <mxGeometry x="160" y="340" width="160" height="170" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-12" value="Polyfoam" style="rounded=0;whiteSpace=wrap;html=1;verticalAlign=middle;horizontal=0;fillColor=#ffe6cc;strokeColor=#d79b00;" parent="1" vertex="1">
          <mxGeometry x="540" y="200" width="20" height="320" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-13" value="Lounge, Dinning, Kitchen, and Work Bench&lt;br&gt;&lt;br&gt;6&#39;x6&#39;x6&#39; minimum&lt;br&gt;" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#d5e8d4;strokeColor=#82b366;" parent="1" vertex="1">
          <mxGeometry x="330" y="200" width="200" height="310" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-15" value="Garage" style="rounded=0;whiteSpace=wrap;html=1;verticalAlign=middle;horizontal=0;fillColor=#a0522d;fontColor=#ffffff;strokeColor=#6D1F00;" parent="1" vertex="1">
          <mxGeometry x="570" y="170" width="40" height="340" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-16" value="Personal Storage" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#60a917;fontColor=#ffffff;strokeColor=#2D7600;" parent="1" vertex="1">
          <mxGeometry x="160" y="200" width="160" height="130" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-18" value="Polyfoam" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#ffe6cc;strokeColor=#d79b00;" parent="1" vertex="1">
          <mxGeometry x="130" y="170" width="430" height="20" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-20" value="Trailer Shell" style="rounded=0;whiteSpace=wrap;html=1;verticalAlign=middle;horizontal=0;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;" parent="1" vertex="1">
          <mxGeometry x="100" y="160" width="20" height="450" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-21" value="Trailer Shell" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#f5f5f5;fontColor=#333333;strokeColor=#666666;" parent="1" vertex="1">
          <mxGeometry x="100" y="140" width="540" height="20" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-23" value="Solar Panels" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#f8cecc;strokeColor=#b85450;" parent="1" vertex="1">
          <mxGeometry x="100" y="110" width="340" height="20" as="geometry" />
        </mxCell>
        <mxCell id="s75VNq6IoDT7e8uqfi6e-25" value="Additional Storage" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#60a917;fontColor=#ffffff;strokeColor=#2D7600;" parent="1" vertex="1">
          <mxGeometry x="450" y="80" width="190" height="50" as="geometry" />
        </mxCell>
      </root>
    </mxGraphModel>
  </diagram>
  <diagram id="QYtDQSU8cPqpaZtphzk8" name="Plumbing System">
    <mxGraphModel dx="1900" dy="570" grid="1" gridSize="10" guides="1" tooltips="1" connect="1" arrows="1" fold="1" page="1" pageScale="1" pageWidth="850" pageHeight="1100" math="0" shadow="0">
      <root>
        <mxCell id="0" />
        <mxCell id="1" parent="0" />
        <mxCell id="0yIEMMHDmGWLwMvTvP7--14" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;exitX=0.25;exitY=0;exitDx=0;exitDy=0;" parent="1" source="0yIEMMHDmGWLwMvTvP7--1" target="0yIEMMHDmGWLwMvTvP7--16" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--38" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" parent="1" source="0yIEMMHDmGWLwMvTvP7--1" target="0yIEMMHDmGWLwMvTvP7--37" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--1" value="Grey Water Tank" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#0050ef;fontColor=#ffffff;strokeColor=#001DBC;" parent="1" vertex="1">
          <mxGeometry x="110" y="570" width="360" height="80" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--33" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--2" target="0yIEMMHDmGWLwMvTvP7--32" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--2" value="Fresh Drinking &lt;br&gt;Water Tank" style="rounded=1;whiteSpace=wrap;html=1;fillColor=#dae8fc;strokeColor=#6c8ebf;" parent="1" vertex="1">
          <mxGeometry x="482.5" y="205" width="85" height="90" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--5" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--3" target="0yIEMMHDmGWLwMvTvP7--4" edge="1">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="30" y="190" />
              <mxPoint x="30" y="190" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--3" value="Roof Rain Collection" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#6a00ff;fontColor=#ffffff;strokeColor=#3700CC;" parent="1" vertex="1">
          <mxGeometry x="-70" y="140" width="670" height="20" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--6" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=0;entryY=0.25;entryDx=0;entryDy=0;" parent="1" source="0yIEMMHDmGWLwMvTvP7--4" target="0yIEMMHDmGWLwMvTvP7--1" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--4" value="Gravity Filter" style="triangle;whiteSpace=wrap;html=1;shape=step;perimeter=stepPerimeter;size=0.09090909090909091;fillColor=#6a00ff;fontColor=#ffffff;strokeColor=#3700CC;" parent="1" vertex="1">
          <mxGeometry x="-20" y="570" width="100" height="40" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--8" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--7" target="0yIEMMHDmGWLwMvTvP7--1" edge="1">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="60" y="620" />
              <mxPoint x="60" y="620" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--7" value="Shore Fresh Water Inlet" style="ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=#0050ef;fontColor=#ffffff;strokeColor=#001DBC;" parent="1" vertex="1">
          <mxGeometry x="-130" y="630" width="80" height="80" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--23" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--9" target="0yIEMMHDmGWLwMvTvP7--10" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--9" value="Shower&lt;br&gt;Head" style="ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=#cce5ff;strokeColor=#36393d;" parent="1" vertex="1">
          <mxGeometry x="212.5" y="340" width="60" height="60" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--24" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--10" target="0yIEMMHDmGWLwMvTvP7--1" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--10" value="Shower Drain" style="ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=#0050ef;strokeColor=#001DBC;fontColor=#ffffff;" parent="1" vertex="1">
          <mxGeometry x="212.5" y="440" width="60" height="60" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--26" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--11" target="0yIEMMHDmGWLwMvTvP7--12" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--11" value="Sink Faucet" style="ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=#cce5ff;strokeColor=#36393d;" parent="1" vertex="1">
          <mxGeometry x="307.5" y="340" width="60" height="60" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--27" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--12" target="0yIEMMHDmGWLwMvTvP7--1" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--12" value="Faucet Drain" style="ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=#0050ef;fontColor=#ffffff;strokeColor=#001DBC;" parent="1" vertex="1">
          <mxGeometry x="307.5" y="440" width="60" height="60" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--40" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--13" target="0yIEMMHDmGWLwMvTvP7--39" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--13" value="Pump #1" style="shape=rhombus;whiteSpace=wrap;html=1;boundedLbl=1;backgroundOutline=1;darkOpacity=0.05;darkOpacity2=0.1;perimeter=rhombusPerimeter;fillColor=#1ba1e2;fontColor=#ffffff;strokeColor=#006EAF;" parent="1" vertex="1">
          <mxGeometry x="80" y="350" width="90" height="80" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--17" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--16" target="0yIEMMHDmGWLwMvTvP7--13" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--16" value="Filter #1" style="triangle;whiteSpace=wrap;html=1;shape=step;perimeter=stepPerimeter;fillColor=#0050ef;fontColor=#ffffff;strokeColor=#001DBC;" parent="1" vertex="1">
          <mxGeometry x="85" y="450" width="80" height="40" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--21" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=0.5;entryY=0;entryDx=0;entryDy=0;" parent="1" source="0yIEMMHDmGWLwMvTvP7--18" target="0yIEMMHDmGWLwMvTvP7--20" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--18" value="Filter #2" style="triangle;whiteSpace=wrap;html=1;shape=step;perimeter=stepPerimeter;fillColor=#1ba1e2;fontColor=#ffffff;strokeColor=#006EAF;" parent="1" vertex="1">
          <mxGeometry x="80" y="170" width="90" height="40" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--22" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--20" target="0yIEMMHDmGWLwMvTvP7--9" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--25" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--20" target="0yIEMMHDmGWLwMvTvP7--11" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--30" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--20" target="0yIEMMHDmGWLwMvTvP7--28" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--20" value="Pump #2" style="shape=rhombus;whiteSpace=wrap;html=1;boundedLbl=1;backgroundOutline=1;darkOpacity=0.05;darkOpacity2=0.1;perimeter=rhombusPerimeter;fillColor=#cce5ff;strokeColor=#36393d;" parent="1" vertex="1">
          <mxGeometry x="197.5" y="210" width="90" height="80" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--31" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--28" target="0yIEMMHDmGWLwMvTvP7--2" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--28" value="Filter #2" style="triangle;whiteSpace=wrap;html=1;shape=step;perimeter=stepPerimeter;size=0.1;fillColor=#cce5ff;strokeColor=#36393d;" parent="1" vertex="1">
          <mxGeometry x="367.5" y="230" width="70" height="40" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--35" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;entryX=1;entryY=0.5;entryDx=0;entryDy=0;" parent="1" source="0yIEMMHDmGWLwMvTvP7--32" target="0yIEMMHDmGWLwMvTvP7--34" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--32" value="Pump #3" style="shape=rhombus;whiteSpace=wrap;html=1;boundedLbl=1;backgroundOutline=1;darkOpacity=0.05;darkOpacity2=0.1;perimeter=rhombusPerimeter;fillColor=#dae8fc;strokeColor=#6c8ebf;" parent="1" vertex="1">
          <mxGeometry x="480" y="330" width="90" height="80" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--36" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--34" target="0yIEMMHDmGWLwMvTvP7--12" edge="1">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="425" y="470" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--34" value="Fresh Water Faucet" style="ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=#dae8fc;strokeColor=#6c8ebf;" parent="1" vertex="1">
          <mxGeometry x="395" y="340" width="60" height="60" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--37" value="Main Drain Outlet" style="ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=#0050ef;fontColor=#ffffff;strokeColor=#001DBC;" parent="1" vertex="1">
          <mxGeometry x="490" y="680" width="70" height="70" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--41" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--39" target="0yIEMMHDmGWLwMvTvP7--18" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--39" value="Heated Floors" style="rounded=1;whiteSpace=wrap;html=1;fillColor=#1ba1e2;fontColor=#ffffff;strokeColor=#006EAF;" parent="1" vertex="1">
          <mxGeometry x="80" y="250" width="90" height="70" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--49" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--42" target="0yIEMMHDmGWLwMvTvP7--1" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--42" value="Pump #1" style="shape=rhombus;whiteSpace=wrap;html=1;boundedLbl=1;backgroundOutline=1;darkOpacity=0.05;darkOpacity2=0.1;perimeter=rhombusPerimeter;fillColor=#0050ef;fontColor=#ffffff;strokeColor=#001DBC;" parent="1" vertex="1">
          <mxGeometry x="245" y="675" width="90" height="80" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--48" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--43" target="0yIEMMHDmGWLwMvTvP7--42" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--43" value="External Source&lt;br&gt;Filter" style="shape=step;perimeter=stepPerimeter;whiteSpace=wrap;html=1;fixedSize=1;size=10;fillColor=#6a00ff;fontColor=#ffffff;strokeColor=#3700CC;" parent="1" vertex="1">
          <mxGeometry x="85" y="695" width="120" height="40" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--46" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" parent="1" source="0yIEMMHDmGWLwMvTvP7--45" target="0yIEMMHDmGWLwMvTvP7--43" edge="1">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="0yIEMMHDmGWLwMvTvP7--45" value="Hose Connection" style="ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=#6a00ff;fontColor=#ffffff;strokeColor=#3700CC;" parent="1" vertex="1">
          <mxGeometry x="-30" y="677.5" width="75" height="75" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-1" value="" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;noEdgeStyle=1;orthogonal=1;fillColor=#1ba1e2;strokeColor=#006EAF;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-2" target="mQXq2xOPe4O9ozQ32qzQ-29">
          <mxGeometry relative="1" as="geometry">
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-2" value="Fresh" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#1ba1e2;fontColor=#ffffff;strokeColor=#006EAF;" vertex="1" parent="1">
          <mxGeometry x="170" y="922.5" width="173.62" height="55" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-3" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;noEdgeStyle=1;orthogonal=1;fillColor=#76608a;strokeColor=#432D57;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-14" target="mQXq2xOPe4O9ozQ32qzQ-10">
          <mxGeometry relative="1" as="geometry">
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-4" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;orthogonal=1;entryX=0.5;entryY=1;entryDx=0;entryDy=0;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-5" target="mQXq2xOPe4O9ozQ32qzQ-14">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-5" value="Grey" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#76608a;fontColor=#ffffff;strokeColor=#432D57;" vertex="1" parent="1">
          <mxGeometry x="6.810000000000002" y="1290" width="530" height="60" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-6" value="Black" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#fad7ac;strokeColor=#b46504;" vertex="1" parent="1">
          <mxGeometry x="20" y="1490" width="530" height="140" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-7" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-8" target="mQXq2xOPe4O9ozQ32qzQ-6">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="-70" y="1380" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-8" value="Toilet" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#fad7ac;strokeColor=#b46504;" vertex="1" parent="1">
          <mxGeometry x="-110" y="1055" width="85" height="70" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-9" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;fillColor=#1ba1e2;strokeColor=#006EAF;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-10" target="mQXq2xOPe4O9ozQ32qzQ-2">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-10" value="Stage 2 Filter" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#1ba1e2;fontColor=#ffffff;strokeColor=#006EAF;" vertex="1" parent="1">
          <mxGeometry x="30" y="920" width="110" height="60" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-11" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;noEdgeStyle=1;orthogonal=1;entryX=0;entryY=0.5;entryDx=0;entryDy=0;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-14" target="mQXq2xOPe4O9ozQ32qzQ-25">
          <mxGeometry relative="1" as="geometry">
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-12" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;noEdgeStyle=1;orthogonal=1;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-14" target="mQXq2xOPe4O9ozQ32qzQ-20">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="190" y="1098" as="sourcePoint" />
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-13" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;exitX=1;exitY=0.5;exitDx=0;exitDy=0;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-14" target="mQXq2xOPe4O9ozQ32qzQ-8">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-14" value="Stage 1 Filter" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#76608a;fontColor=#ffffff;strokeColor=#432D57;" vertex="1" parent="1">
          <mxGeometry x="40" y="1055" width="80" height="70" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-15" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-16" target="mQXq2xOPe4O9ozQ32qzQ-5">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="400" y="1240" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-16" value="Sink Drain" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#76608a;fontColor=#ffffff;strokeColor=#432D57;" vertex="1" parent="1">
          <mxGeometry x="380" y="1080" width="45" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-17" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-18" target="mQXq2xOPe4O9ozQ32qzQ-5">
          <mxGeometry relative="1" as="geometry">
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-18" value="Shower Pan" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#76608a;fontColor=#ffffff;strokeColor=#432D57;" vertex="1" parent="1">
          <mxGeometry x="203.62" y="1220" width="136.38" height="30" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-19" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;noEdgeStyle=1;orthogonal=1;dashed=1;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-20" target="mQXq2xOPe4O9ozQ32qzQ-16">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="290" y="1025" as="sourcePoint" />
            <mxPoint x="540" y="1180" as="targetPoint" />
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-20" value="Faucet" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#76608a;fontColor=#ffffff;strokeColor=#432D57;" vertex="1" parent="1">
          <mxGeometry x="203.62" y="1000" width="136.38" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-21" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;noEdgeStyle=1;orthogonal=1;dashed=1;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-22" target="mQXq2xOPe4O9ozQ32qzQ-18">
          <mxGeometry relative="1" as="geometry">
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-22" value="Shower head" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#76608a;fontColor=#ffffff;strokeColor=#432D57;" vertex="1" parent="1">
          <mxGeometry x="203.62" y="1140" width="136.38" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-23" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;noEdgeStyle=1;orthogonal=1;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-25" target="mQXq2xOPe4O9ozQ32qzQ-20">
          <mxGeometry relative="1" as="geometry">
            <mxPoint x="179.44444444444434" y="1100" as="targetPoint" />
            <Array as="points" />
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-24" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-25" target="mQXq2xOPe4O9ozQ32qzQ-22">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-25" value="Hot Water Heater" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#76608a;fontColor=#ffffff;strokeColor=#432D57;" vertex="1" parent="1">
          <mxGeometry x="203.62" y="1070" width="136.38" height="40" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-26" style="edgeStyle=orthogonalEdgeStyle;rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-27" target="mQXq2xOPe4O9ozQ32qzQ-5">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="600" y="880" />
              <mxPoint x="600" y="1320" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-27" value="Rain Water Collector" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#76608a;fontColor=#ffffff;strokeColor=#432D57;" vertex="1" parent="1">
          <mxGeometry x="-30" y="870" width="610" height="30" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-28" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-29" target="mQXq2xOPe4O9ozQ32qzQ-16">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-29" value="Drinking Water" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#1ba1e2;fontColor=#ffffff;strokeColor=#006EAF;" vertex="1" parent="1">
          <mxGeometry x="372.5" y="925" width="60" height="55" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-30" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-31" target="mQXq2xOPe4O9ozQ32qzQ-5">
          <mxGeometry relative="1" as="geometry">
            <Array as="points">
              <mxPoint x="475" y="1250" />
            </Array>
          </mxGeometry>
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-31" value="Water Inlet" style="rounded=0;whiteSpace=wrap;html=1;fillColor=#76608a;fontColor=#ffffff;strokeColor=#432D57;" vertex="1" parent="1">
          <mxGeometry x="500" y="1140" width="50" height="50" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-32" style="rounded=0;orthogonalLoop=1;jettySize=auto;html=1;" edge="1" parent="1" source="mQXq2xOPe4O9ozQ32qzQ-33" target="mQXq2xOPe4O9ozQ32qzQ-25">
          <mxGeometry relative="1" as="geometry" />
        </mxCell>
        <mxCell id="mQXq2xOPe4O9ozQ32qzQ-33" value="Gas" style="ellipse;whiteSpace=wrap;html=1;aspect=fixed;fillColor=#fa6800;fontColor=#000000;strokeColor=#C73500;" vertex="1" parent="1">
          <mxGeometry x="120" y="1150" width="40" height="40" as="geometry" />
        </mxCell>
      </root>
    </mxGraphModel>
  </diagram>
</mxfile>
